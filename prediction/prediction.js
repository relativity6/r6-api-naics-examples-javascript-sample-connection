function prediction() {

    const url = "https://api.usemarvin.ai/naics/naics/search/"

    const data = {
        "token": "YOUR_TOKEN_HERE",
        "name": "Apple Inc.",
        "address": "One Apple Park Way",
        "state": "CA"
    }

    fetch(url, { 
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        })
        .then(resp => {
             return resp.json()
             })
        .then(res =>
             console.log(res))
        .catch(err => 
            console.log(err)
            )
}

# EXAMPLE - HTTP REQUEST TO API NAICS IDENTIFIER

## Getting Started

Following these instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Usage

In order to run the process, make sure that you have the proper token to send requests. Once you have the token, follow the next steps to execute the code:

1. Replace '**YOUR_TOKEN_HERE**' with your **token**


## Built With

* [Visual Studio Code](https://code.visualstudio.com/download) 
* [Github](https://github.com/) - Code Hosting Platform for Version Control

## Authors

* **Abelardo Colorado Diaz** - *Developer* - abelardo@relativity6.com

## License

This project is private, owned by "Relativity6"